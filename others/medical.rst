Medical Insutitution
=====================

Dentist
--------

.. toctree::
   :glob:
   :maxdepth: 2

   dentist/*


General Practitioner(GP)
-------------------------
If you feel sick, you would go there first. But, Just a common cold, you shouldn't go there.
Because, the doctor says that 'You should take a medicine, then take a rest.'.

.. toctree::
   :glob:
   :maxdepth: 2

   gp/*

