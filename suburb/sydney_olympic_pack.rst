Sydney Olympic Park
===============
Sydney Olympic Park is a big park where was held Sydney Olympic.


Food
-----

.. toctree::
   :glob:
   :maxdepth: 1

   olympic_park/food/*

Park
-----

.. toctree::
   :glob:
   :maxdepth: 1

   olympic_park/park/*
