.. Local Information documentation master file, created by
   sphinx-quickstart on Sun Feb 28 20:49:33 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Local Information's documentation!
=============================================

Local Information
------------------

.. toctree::
   :glob:
   :maxdepth: 3

   suburb/*

Melbourne Information
----------------------

.. toctree::
   :glob:
   :maxdepth: 3

   melbourne/*

Event Information
-----------------

.. toctree::
   :glob:
   :maxdepth: 2

   events/*

Others Information
----------------------

.. toctree::
   :glob:
   :maxdepth: 3

   others/*
